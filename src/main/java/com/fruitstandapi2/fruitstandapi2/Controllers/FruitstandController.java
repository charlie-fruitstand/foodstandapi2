package com.fruitstandapi2.fruitstandapi2.Controllers;

import java.util.List;

import com.fruitstandapi2.fruitstandapi2.Models.Fruitstand;
import com.fruitstandapi2.fruitstandapi2.Respositories.FruitstandRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping(value = "fruitstand")
@CrossOrigin
public class FruitstandController {
    private final FruitstandRepository fruitstandRepo;

    public FruitstandController(FruitstandRepository fruitstandRepo) {
        this.fruitstandRepo = fruitstandRepo;
    }

    @GetMapping("/fruits")
    public List<Fruitstand> viewAll(Model model, String allFruit) {
        List<Fruitstand> fruitList = fruitstandRepo.findAll();
        model.addAttribute("noFruitsFound", fruitList.size() == 0);
        model.addAttribute("fruits", fruitList);
        return fruitList;
    }

    @PostMapping("/fruits/add-fruit")
    public List<Fruitstand> newFruit(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description,
                            @RequestParam(name = "location") String location,
                            @RequestParam(name = "color") String color,
                            @RequestParam(name = "shape") String shape){

        Fruitstand fruitToBeSaved = new Fruitstand();
        fruitToBeSaved.setName(name);
        fruitToBeSaved.setColor(color);
        fruitToBeSaved.setDescription(description);
        fruitToBeSaved.setLocation(location);
        fruitToBeSaved.setShape(shape);
        fruitstandRepo.save(fruitToBeSaved);
        return fruitstandRepo.findAll();
    }

    @GetMapping("/fruits/{id}")
    public String viewFruit(@PathVariable long id, Model model, String oneFruit) {
        Fruitstand fruitstand = fruitstandRepo.getOne(id);
        model.addAttribute("fruitId", id);
        model.addAttribute("fruit", fruitstand);
        return oneFruit;
    }

    @PostMapping("/fruits/{id}/edit")
    public void updateFruit(@PathVariable long id, 
                            @RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description,
                            @RequestParam(name = "location") String location,
                            @RequestParam(name = "color") String color,
                            @RequestParam(name = "shape") String shape
                            ) {
        Fruitstand foundFruit = fruitstandRepo.getOne(id);
        foundFruit.setName(name);
        foundFruit.setDescription(description);
        foundFruit.setLocation(location);
        foundFruit.setColor(color);
        foundFruit.setShape(shape);
        fruitstandRepo.save(foundFruit);

        // System.out.println(id);
        // System.out.println(name);
        // System.out.println(description);
        // System.out.println(location);
        // System.out.println(color);
        // System.out.println(shape);

        // return postEditFruit;
    }

    @PostMapping("/fruits/{id}/remove")
    public void removeFruit(@PathVariable String id,
                            @RequestParam(name = "del") String del) {

        long id2 = Long.parseLong(del);

        fruitstandRepo.deleteById(id2);
    }

    @DeleteMapping("/fruits/{id}/remove")
    public void removeFruit2(@PathVariable long id){
        fruitstandRepo.deleteById(id);
        // System.out.println(id);
    }

    

}
