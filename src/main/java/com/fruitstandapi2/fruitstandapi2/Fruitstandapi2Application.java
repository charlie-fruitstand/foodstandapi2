package com.fruitstandapi2.fruitstandapi2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fruitstandapi2Application {

	public static void main(String[] args) {
		SpringApplication.run(Fruitstandapi2Application.class, args);
	}

}
