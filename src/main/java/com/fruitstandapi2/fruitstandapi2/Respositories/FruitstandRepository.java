package com.fruitstandapi2.fruitstandapi2.Respositories;

import com.fruitstandapi2.fruitstandapi2.Models.Fruitstand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FruitstandRepository extends JpaRepository<Fruitstand, Long> {
}
